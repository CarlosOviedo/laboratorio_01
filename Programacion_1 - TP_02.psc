Proceso TP_2
	
	Definir legajo,opcion,result,posicion,Hora_ingresada,Minuto_ingresado,i Como Entero;
	Definir HoraIngreso,HoraEgreso,MinutoIngreso,MinutoEgreso Como Entero;
	Definir Users,Pass Como Entero;
	Dimension Users[3],Pass[3],HoraIngreso[3],HoraEgreso[3],MinutoIngreso[3],MinutoEgreso[3];
	
	/// Legajos y Contraseņas
	// Se utilizan las posiciones para
	Users[0]<-000;
	Pass[0]<-123;
	
	Users[1]<-001;
	Pass[1]<-456;
	
	Users[2]<-002;
	Pass[2]<-789;
	
	/// Inicializacion de Variables
	Hora_ingresada<--1;
	Minuto_ingresado<--1;
	posicion<--1;
	
	/// Inicializacion de Vectores Tiempo
	Para i<-0 Hasta 2 Con Paso 1 Hacer
		HoraIngreso[i]<--1;
		HoraEgreso[i]<--1;
		MinutoIngreso[i]<--1;
		MinutoEgreso[i]<--1;
	FinPara
	
	Repetir
		opcion<-0;
		
		/// MENU: con 3 Opciones Registrar Ingreso - Registrar Egreso - Registrar Salir
		Mientras opcion<1 O opcion>3 Hacer
			Escribir "Elija la Opcion correcta";
			Escribir "Opcion 01: Registrar Ingreso";
			Escribir "Opcion 02: Registrar Egreso";
			Escribir "Opcion 03: Registrar Salir";
			Leer opcion;
			Si opcion<1 O opcion>3 Entonces
				Escribir "Error: Valor Ingresado Incorrecto";
				Escribir "(Debe ingresar un valor entre 1, 2 o 3)";
			FinSi
		FinMientras

		Limpiar Pantalla;
		
		// Menu con SEGUN ___ Hacer
		Segun opcion Hacer
			/// Primera Opcion: Ingreso
			1:
				Escribir "- Registrar Ingreso - ";
				// Realizamos el Login con la funcion Login
				// La variable posicion es enviada por referencia
				result<-Login (Users,Pass,posicion);
				//Si la funcion devuelve el 2: El legajo y la contraseņa es la correcta
				Si result=1 Entonces
					Escribir "Ingrese la Hora de Ingreso";
					// La Funcion Tiempo se encarga de la carga y validacion de la Hora y Minuto
					//Se envia las variables por referencia
					Tiempo(Hora_ingresada,Minuto_ingresado); 
					Limpiar Pantalla;
					HoraIngreso[posicion]<-Hora_ingresada;
					MinutoIngreso[posicion]<-Minuto_ingresado;
					//Se puede verificar los datos ingresados sean correctos
					Escribir "Legajo: ",Users[posicion];
					Escribir "Hora de Ingreso: " Sin Saltar;
					Escribir HoraIngreso[posicion] Sin Saltar;
					Escribir " : ", MinutoIngreso[posicion];
					mensaje ;
				FinSi
			/// Segunda Opcion: Egreso
			2:
				Escribir "- Registrar Egreso - ";
				// La Funcion Tiempo se encarga de la carga y validacion de la Hora y Minuto
				//Se envia las variables por referencia
				result<-Login (Users,Pass,posicion);
				//Si la funcion devuelve el 2: El legajo y la contraseņa es la correcta
				Si result=1 Entonces
					// La Funcion Tiempo se encarga de la carga y validacion de la Hora y Minuto
					//Se envia las variables por referencia
					Tiempo(Hora_ingresada,Minuto_ingresado); //Se envia las variables por referencia
					Limpiar Pantalla;
					HoraEgreso[posicion]<-Hora_ingresada;
					MinutoEgreso[posicion]<-Minuto_ingresado;
					Escribir "Legajo: ",Users[posicion];
					Escribir "Hora de Egreso: " Sin Saltar;
					Escribir HoraEgreso[posicion] Sin Saltar;
					Escribir " : ", MinutoEgreso[posicion];
					mensaje ;
				FinSi
			/// Tercera Opcion: Salir
			3:
				posicion<--1; // Le ingresamos un valor incorrecto para validar luego..
				Escribir "- SALIR - ";
				Escribir "Ingrese el Legajo: " Sin Saltar;
				Leer legajo;
				posicion<-Validacion_user (legajo,Users);
				Si posicion=-1 Entonces
					Escribir "Legajo Invalido";
					mensaje ;
				FinSi
				
				Si posicion!=-1 Y (HoraIngreso[posicion]=-1 o MinutoIngreso[posicion]=-1) Entonces
					Escribir "Error: ", Sin Saltar;
					Escribir "No Ingreso la Hora de Ingreso";
					opcion<-0; // Nos Permite Volver al Menu y no Salir
				FinSi
				Si posicion!=-1 Y (HoraEgreso[posicion]=-1 o MinutoEgreso[posicion]=-1) Entonces
					Escribir "Error: ", Sin Saltar;
					Escribir "No Ingreso la Hora de Egreso";
					opcion<-0; // Nos Permite Volver al Menu y no Salir
				FinSi
				Si ((posicion!=-1) Y (HoraIngreso[posicion]!=-1) Y (MinutoIngreso[posicion]!=-1) Y (HoraEgreso[posicion]!=-1) Y (MinutoEgreso[posicion]!=-1)) Entonces
					//Si se ingreso un legajo valido y se cargo la horas mostramos la informacion antes de salir ...
					Escribir "Legajo: ",Users[posicion];
					Escribir "Hora de Ingreso: " Sin Saltar;
					Escribir HoraIngreso[posicion] Sin Saltar;
					Escribir " : ", MinutoIngreso[posicion];	
					Escribir "Hora de Egreso: " Sin Saltar;
					Escribir HoraEgreso[posicion] Sin Saltar;
					Escribir " : ", MinutoEgreso[posicion];	
				FinSi
				mensaje ;
		FinSegun
	Hasta Que opcion=3;// Si opcion no es 3 Se vuelve a mostrar el MENU
	
FinProceso

/// Funcion: LOGIN
Funcion result <-Login (Users,Pass,posicion Por Referencia)
	Definir i,pos_user,pos_pass,ing_user,ing_pass,result Como Entero;
	//Inicializamos Variables en -1 para evaluarlas luego
	pos_user<--1;
	pos_pass<--2;
	
	Escribir "Legajo: "Sin Saltar;
	Leer ing_user; 
	pos_user<-Validacion_user (ing_user,Users);
	
	Si pos_user!=-1 Entonces 
		Escribir "Contraseņa: "Sin Saltar;
		Leer ing_pass;
		pos_pass<-Validacion_pass (ing_pass,Pass, pos_user);
	FinSi
	
	Si ((pos_user!=-1) Y (pos_user!=-1)) Entonces
		posicion <-pos_user;
		result<-1;
	FinSi
	Si (pos_user=-1) Entonces
		Escribir "Legajo Ingresado Incorrecto";
		mensaje ;
		Limpiar Pantalla;
		result<-0;
		
	FinSi
	Si (pos_pass=-1) Entonces
		Escribir "Contraseņa Ingresada Incorrecto";
		mensaje ;
		result<-0;
	FinSi
FinFuncion

/// Funcion: Para Validar el Usuario Ingresado
Funcion val_user<-Validacion_user (ing_user,Users)
	Definir i,val_user,aux_user Como Entero;
	val_user<--1;
	Para i<-0 Hasta 2 Con Paso 1 Hacer
		SI Users[i]=ing_user Entonces
			aux_user<-1;
			val_user<-i; // Se guarda la posicion del usuario correcto
		FinSi
	FinPara
FinFuncion

/// Funcion: Para validar la contraseņa ingresada
Funcion val_pass<-Validacion_pass (ing_pass,Pass,pos_user)
	Definir val_pass Como Entero;
	val_pass<--1;
	SI Pass[pos_user]=ing_pass Entonces
		val_pass<-pos_user; 
	FinSi
FinFuncion

/// Funcion: Para Validar la Hora y los Minutos
Funcion Tiempo(Hora_ingresada Por Referencia,Minuto_ingresado Por Referencia)

	Repetir
		Escribir "Hora [0 a 23]: " Sin Saltar;
		Leer Hora_ingresada;
		Si Hora_ingresada<0 o Hora_ingresada>23 Entonces 
			Escribir "Error: La Hora debe ser entre 0 y 23";
		FinSi
	Hasta Que Hora_ingresada>=0 Y Hora_ingresada<=23;
	
	Repetir
		Escribir "Minutos [0 a 59]: " Sin Saltar;
		Leer Minuto_ingresado;	
		Si Minuto_ingresado<0 o Minuto_ingresado>59 Entonces
			Escribir "Error: La Hora debe ser entre 0 y 59";
		FinSi
	Hasta Que Minuto_ingresado>=0 Y Minuto_ingresado<=59;
FinFuncion

/// Funcion: Para mostrar mensaje de salida
Funcion mensaje 
	Definir i Como Entero;
	Escribir "Presione una tecla para continuar ...";
	Leer i; // Nos permite darnos el tiempo para leer la pantalla y luego limpiarla
	Limpiar Pantalla;
FinFuncion
	