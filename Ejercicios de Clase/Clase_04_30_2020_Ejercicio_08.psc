Proceso sin_titulo
	Definir Vector,Num,i,Positivos,Negativos,Ceros Como Entero;
	Positivos<-0;
	Negativos<-0;
	Ceros<-0;
	Dimension Vector[20];
	
	Para i<-0 Hasta 19 Con Paso 1 Hacer
		Vector[i] <- Aleatorio(-10,10) ;
		Si Vector[i]>0 Entonces
			Positivos<-Positivos+1;
		FinSi
		Si Vector[i]<0 Entonces
			Negativos<-Negativos+1;
		FinSi
		Si Vector[i]=0 Entonces
			Ceros<-Ceros+1;
		FinSi
	FinPara
	//Visualizacion del Vector
	Escribir "El Vector cargado es:";
	Para i<-0 Hasta 19 Con Paso 1 Hacer
		Escribir Vector[i]," | " Sin Saltar;
	FinPara
	Escribir "";
	
	//Visualizacion de los Ceros
	Escribir "Ceros: " Sin Saltar;
	Para i<-1 Hasta Ceros Con Paso 1 Hacer
		Escribir "*", Sin Saltar;
	FinPara
	Escribir "";
	
	//Visualizacion de los Positivos
	Escribir "Positivos: " Sin Saltar;
	Para i<-1 Hasta Positivos Con Paso 1 Hacer
		Escribir "*", Sin Saltar;
	FinPara
	Escribir "";
	
	//Visualizacion de los Negativos
	Escribir "Negativos: "Sin Saltar;
	Para i<-1 Hasta Negativos Con Paso 1 Hacer
		Escribir "*", Sin Saltar;
	FinPara
	Escribir "";
FinProceso
