Proceso Arreglo_Aleatorio
	Definir Vector,i,j,num,flag,posicion,contador Como Entero;
	Dimension Vector[100];
	flag<-0;
	
	//Punto a:
	Para i<-0 Hasta 99 Con Paso 1 Hacer
		Vector[i]<-ALEATORIO(0,20); 
	FinPara
	
	Para i<-0 Hasta 99 Con Paso 1 Hacer
		Escribir "Vector [",i+1,"]: ",Vector[i];
	FinPara
	//Punto b:
	Escribir "Ingrese un numero: ", Sin Saltar;
	Leer num;
	Para i<-0 Hasta 99 Con Paso 1 Hacer
		Si Vector[i]=num Entonces
			flag<-1;
			FinSi
	FinPara
	Si flag=1 Entonces
		Escribir "El numero ingresado ",num," se encuentra en el vector";
	SiNo
		Escribir "El numero ingresado ",num," no se encuentra en el vector";
	FinSi
	//Punto C:
	Escribir "Que posicion decea cambiar: ",Sin Saltar;
	Leer posicion;
	Repetir
		Escribir "Ingrese un numero: [Entre 0 y 20] ",Sin Saltar;
		Leer num;
		Si num<=0 o num >=20 Entonces
			Escribir "Error: Numero Incorrecto";
		FinSi
	Hasta Que num>=0 Y num <=20;

	Vector[posicion-1]<-(num);
	Limpiar Pantalla;
	Escribir "El Nuevo vector queda:";
	Para i<-0 Hasta 99 Con Paso 1 Hacer
		Escribir "Vector [",i+1,"]: ",Vector[i];
	FinPara
	
	//Punto D:
	Para j<-0 Hasta 20 Con Paso 1 Hacer
		num<-j;
		contador<-0;
		Para i<-0 Hasta 99 Con Paso 1 Hacer
			Si (j=Vector[i])  Entonces
				contador<-contador+1;
			FinSi
		FinPara	
		Si contador!=0 Entonces
			Escribir "El numero ",num," se repite ",contador," veces";
		FinSi
	FinPara
		
FinProceso
