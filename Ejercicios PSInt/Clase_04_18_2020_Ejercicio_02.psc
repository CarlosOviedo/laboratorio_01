Proceso Pitagoras
	Definir Cateto_A Como Real;
	Definir Cateto_B Como Real;
	Definir Hipotenusa Como Real;
	
	Cateto_A <- 0;
	Cateto_B <- 0;
	Hipotenusa <- 0;
	
	Escribir 'Ingresar el valor de cada lado del Triangulo';
	Escribir 'Cateto A:';
	Leer Cateto_A;
	Escribir 'Cateto B:';
	Leer Cateto_B;
	
	Hipotenusa <- rc(Cateto_A^2 + Cateto_B^2);
	Escribir 'Hipotenusa:';
	Escribir Hipotenusa;
FinProceso
