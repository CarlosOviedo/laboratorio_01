#include <stdio.h>
#include <math.h>

int main ()
{
float X_01,Y_01,X_02,Y_02,d;
X_01=0;
Y_01=0;
X_02=0;
Y_02=0;
printf ("El programa calcula la distancia entre dos puntos\nIngrese el Primer Punto X_01,Y_01\n");
printf("X_01: ");
scanf("%f",&X_01);
printf("Y_01: ");
scanf("%f",&Y_01);
printf("Ingrese el Segundo Punto X_02,Y_02\n");
printf("X_02: ");
scanf("%f",&X_02);
printf("Y_02: ");
scanf("%f",&Y_02);

printf("La distancia entre los puntos es:\n");
d= sqrt(pow(fabs(X_02-X_01),2)+pow(fabs(Y_02-Y_01),2));
printf("Distancia entre (X_01: %0.3f ; Y_01: %0.3f) y (X_02: %0.3f ; Y_02: %0.3f) : %0.3f",X_01,Y_01,X_02,Y_02,d);
return 0;
}