Proceso Multiplos
	Definir Num,i Como Entero;

	Escribir "Escriba un numero Entero Positivo";
	Leer Num;
	Escribir "Los Multiplos de: ", Num;
	Para i<-0 Hasta Num Con Paso 1 Hacer
		Si i %3 = 0 Entonces
			Escribir i, "-", Sin Saltar;
		FinSi
	FinPara

	
FinProceso
