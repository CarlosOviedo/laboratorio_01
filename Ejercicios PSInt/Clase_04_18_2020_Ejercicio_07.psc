Proceso Calculo_Pi
	Definir N,i,j Como Entero;
	Definir Bandera Como Logico;
	Definir Numero_Pi Como Real;
	Escribir 'Ingrese el numero de terminos de suma que se utilizara para calcular el numero Pi';
	Leer N;
	Numero_Pi<-0;
	j<-1;
	i<-1;
	Bandera<-Verdadero;
	Mientras j<=N Hacer
		Si i==1 O Bandera==Verdadero
		 Entonces  
			Escribir '+ 1/',( i );
			Numero_Pi<-Numero_Pi + (1/i);
			Bandera<-Falso;
			SiNo
			Escribir '- 1/',( i );
			Numero_Pi<-Numero_Pi - (1/i);
			Bandera<-Verdadero;
		FinSi		
		j<-j+1;
		i<-i+2;
	FinMientras
	Numero_Pi<- 4 * Numero_Pi;
	Escribir 'El numero Pi es: ', Numero_Pi;
FinProceso