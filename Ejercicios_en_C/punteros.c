#include <stdio.h>


char *invertir(char *dest, char *orig);
char origen[40] = "Hola Mundo!"; 
char destino[40]; // !odnuM aloH

int main()
{
    int i;
    printf("Direccion Destino: %d \n",(unsigned int)&destino);
    printf("Direccion Origen:  %d \n",(unsigned int)&origen);
    invertir(destino, origen);
    printf ("Dato en Origen:  %s\n", origen);
    printf ("Dato en Destino: %s\n", destino);
    return 0;
};

char *invertir(char *dest, char *orig)
{
  char *p = dest;
  int cnt=0,i;
  while (*orig != '\0')
  {
   *orig++;
   cnt++;
  }
  for(i=1;i<cnt+1;i++) *p++ = *(orig-i);
  *p++='\0';
}
