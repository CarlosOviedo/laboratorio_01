#include <stdio.h>

int main ()
{
    char str [50];
    int i,caracteres_alfabeticos,caracteres_numericos,caracteres_especiales;
    i=0;
    caracteres_alfabeticos=0;
    caracteres_numericos=0;
    caracteres_especiales=0;
    printf ("Introduzca un Texto: \n");
    gets (str);

    while (str[i] != '\0' )
    {
        if(str[i]>=65 && str[i]<=90 || str[i]>=97 && str[i]<=122 )caracteres_alfabeticos++;
        else if(str[i]>=48 && str[i]<=57 )caracteres_numericos++;
        else caracteres_especiales++;

        i++;
    }
       
    printf("El Texto Ingresado:\n");
    printf("%s",str);
    printf("\nContiene:\n");
    printf("Caracteres Alfabeticos: %d\nCaracteres Numericos: %d\nCaracteres Especiales: %d",caracteres_alfabeticos,caracteres_numericos,caracteres_especiales);

       
    return 0;
}