Proceso MatrizIdentidad
	Definir Matriz,i,j,n Como Entero;
	Dimension Matriz[11,11];
	Repetir
		Escribir "Ingrese un numero: [Entre 2 y 10]", Sin Saltar;
		Leer n;
		SI n<2 o n> 10 Entonces
			Escribir "Error: Numero Incorrecto";
		FinSi
	Hasta Que n>=2 Y n<=10;
	
	//Carga de Matriz Identidad
	Para i<-0 Hasta n Con Paso 1 Hacer
		Para j<-0 Hasta n Con Paso 1 Hacer
			Si i=j Entonces
				Matriz[i,j]<-1;
			SiNo
				Matriz[i,j]<-0;
			FinSi
		FinPara
	FinPara
	//Muestra de Matriz Identidad
	Para i<-0 Hasta n Con Paso 1 Hacer
		Para j<-0 Hasta n Con Paso 1 Hacer
			Escribir Matriz[i,j], Sin Saltar;
		FinPara
		Escribir "";
	FinPara
	
	
	
FinProceso
