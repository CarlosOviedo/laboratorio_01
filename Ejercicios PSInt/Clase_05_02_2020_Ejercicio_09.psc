Proceso SumaMatriz
	Definir MatrizA,MatrizB,MatrizC,i,j,n Como Entero;
	Dimension MatrizA[3,3],MatrizB[3,3],MatrizC[3,3];
	
	//Carga de las Matrices con Numeros Aleatorios
	Para i<-0 Hasta 2 Con Paso 1 Hacer
		Para j<-0 Hasta 2 Con Paso 1 Hacer
			MatrizA[i,j]<-ALEATORIO(0,9);
			MatrizB[i,j]<-ALEATORIO(0,9);
		FinPara
	FinPara
	//Suma de las Matrices
	Para i<-0 Hasta 2 Con Paso 1 Hacer
		Para j<-0 Hasta 2 Con Paso 1 Hacer
			MatrizC[i,j]<-MatrizA[i,j]+MatrizB[i,j];
		FinPara
	FinPara
	//Muestra
	Escribir "Matriz A:";
	Para i<-0 Hasta 2 Con Paso 1 Hacer
		Para j<-0 Hasta 2 Con Paso 1 Hacer
			Escribir "[",MatrizA[i,j],"]", Sin Saltar;
		FinPara
		Escribir "";
	FinPara
	Escribir "Matriz B:";
	Para i<-0 Hasta 2 Con Paso 1 Hacer
		Para j<-0 Hasta 2 Con Paso 1 Hacer
			Escribir "[",MatrizB[i,j],"]", Sin Saltar;
		FinPara
		Escribir "";
	FinPara
	Escribir "Matriz C: SUMA DE LAS MATRICES";
	Para i<-0 Hasta 2 Con Paso 1 Hacer
		Para j<-0 Hasta 2 Con Paso 1 Hacer
			Escribir "[",MatrizC[i,j],"]", Sin Saltar;
		FinPara
		Escribir "";
	FinPara
	
	
FinProceso
