Proceso Arreglo_Aleatorio
	Definir i,palabra_larga,palabra_corta,posicion_palabralarga,posicion_palabracorta Como Entero;
	Definir Vector Como Caracter;
	palabra_larga<-0;
	palabra_corta<-0;
	Dimension Vector[5];
	
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		Escribir "Palabra [",i+1,"]: ",Sin Saltar;
		Leer Vector[i];
	FinPara
	Limpiar Pantalla;
	Escribir "Las Palabras Ingresadas:";
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		Escribir "Palabra [",i+1,"]: ",Vector[i];
	FinPara
	//Longitud de Palabras
	palabra_larga<-Longitud(Vector[0]);
	posicion_palabralarga<-0;
	palabra_corta<-Longitud(Vector[0]);
	posicion_palabracorta<-0;
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		SI palabra_larga<Longitud(Vector[i]) Entonces
			palabra_larga<-Longitud(Vector[i]);
			posicion_palabralarga<-i;
		FinSi
		
		SI palabra_corta>Longitud(Vector[i]) Entonces
			palabra_corta<-Longitud(Vector[i]);
			posicion_palabracorta<-i;
		FinSi
	FinPara
	//Muestra en Pantalla.
	Escribir "La Palabra mas larga tiene ",palabra_larga," caracteres";
	Escribir "La Palabra larga es: ",Vector[posicion_palabralarga];
	Escribir "La Palabra mas corta tiene ",palabra_corta," caracteres";
	Escribir "La Palabra corta es: ",Vector[posicion_palabracorta];
FinProceso
