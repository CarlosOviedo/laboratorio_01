Proceso Calculo_IMC
	Definir Edad Como Entero;
	Definir IMC , Altura , Peso como Real;
	
	Escribir 'El riesgo que una persona sufra enfermedades coronarias depende de su edad y su indice de masa corporal';
	Escribir 'Ingrese la Edad';
	Leer Edad;
	Escribir 'Ingrese el peso';
	Leer Peso;
	Escribir 'Ingrese Altura';
	Leer Altura;
	
	IMC <- (Peso/Altura^2);
	
	Si Edad < 45
		Entonces
		Si IMC < 22.0 
			Entonces Escribir 'Riesgo Bajo IMC =',IMC;
			
		FinSi
		
		Si IMC >= 22.0 
			Entonces Escribir 'Riesgo Medio IMC =',IMC;
			
		FinSi
	SiNo
		Si IMC < 22.0 
			Entonces Escribir 'Riesgo Bajo IMC =',IMC;
			
		FinSi
		
		Si IMC >= 22.0 
			Entonces Escribir 'Riesgo alto IMC =',IMC;
			
		FinSi
	FinSi
	
FinProceso
