Proceso MatrizVector
	Definir Matriz,Vector,i,j Como Entero;
	Dimension Matriz[5,5],Vector[5];
	
	//Carga de la Matrice con Numeros Aleatorios
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		Para j<-0 Hasta 4 Con Paso 1 Hacer
			Matriz[i,j]<-ALEATORIO(-55,15);
		FinPara
	FinPara
	//Carga del Vector
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		Para j<-0 Hasta 4 Con Paso 1 Hacer
			Si i=j Entonces
				Vector[i]<-Matriz[i,j];
			SiNo
			FinSi
		FinPara
	FinPara
	//Muestra 
	Escribir "Matriz:";
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		Para j<-0 Hasta 4 Con Paso 1 Hacer
			Escribir "[",Matriz[i,j],"]", Sin Saltar;
		FinPara
		Escribir "";
	FinPara
	Escribir "Vector:";
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		Escribir "[",Vector[i],"]", Sin Saltar;
	FinPara
FinProceso
