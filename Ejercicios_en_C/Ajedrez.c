#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>

char buscarPieza(int i, int j);
char ColorPad(int i, int j);

int main()
{
 char tablero[8][8];
//Asigna Fichas
  for(int i=0;i<8;i++)
  {
    for(int j=0;j<8;j++)
     {
      tablero[i][j]=buscarPieza(i,j);
     }
  }

 printf("Tablero:\n");
 //Imprime Fichas Negras
 for(int i=0;i<2;i++)
  {
    for(int j=0;j<8;j++)
    {
      printf(" %c ",tablero[i][j]);
     
    }
     printf("\n");
  }
  for(int i=0;i<24;i++) printf("_");
  printf("\n");
//Imprime Tablero
 for(int i=0;i<8;i++) 
  {
   for(int j=0;j<8;j++) 
    { 
     ColorPad(i,j);
    }
    printf("\n");
   }
    
    //Imprime Fichas Blancas
 for(int i=0;i<24;i++) printf("_");
 printf("\n");

 for(int i=6;i<8;i++)
  {
    for(int j=0;j<8;j++)
    {
      printf(" %c ",tablero[i][j]);
     
    }
     printf("\n");
  }
 printf("\n\n");

return 0;
}

char buscarPieza(int i, int j){
    switch(i){
        case 0:
            if(j==0 || j==7){ return 'T';}
            if(j==1 || j==6){ return 'C';}
            if(j==2 || j==5){ return 'A';}
            if(j==3){ return 'R';}
            if(j==4){ return 'Y';}
        break;
        case 1:
            return 'P';
        break;
        case 6:
            return 'P';
        break;
        case 7:
            if(j==0 || j==7){ return 'T';}
            if(j==1 || j==6){ return 'C';}
            if(j==2 || j==5){ return 'A';}
            if(j==3){ return 'R';}
            if(j==4){ return 'Y';}
        break;
        default:
            return ' ';
        break;
    }
}

char ColorPad(int i, int j)
{
  if((i+j)%2) printf("%c%c%c",32,32,32);
  else printf("%c%c%c",219,219,219); 
}