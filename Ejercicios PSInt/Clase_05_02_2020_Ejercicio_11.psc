Proceso Ejercicio_11
	Definir Matriz,num,i,j,k,flag,contador Como Entero;
	Dimension Matriz[10,10];
	flag<-0;
	//Carga de la Matrice con Numeros Aleatorios
	//Punto a:
	Para i<-0 Hasta 9 Con Paso 1 Hacer
		Para j<-0 Hasta 9 Con Paso 1 Hacer
			Matriz[i,j]<-ALEATORIO(0,20);
		FinPara
	FinPara
	Escribir "Matriz:";
	Para i<-0 Hasta 9 Con Paso 1 Hacer
		Para j<-0 Hasta 9 Con Paso 1 Hacer
			SI Matriz[i,j]< 10 Entonces
				Escribir "[0",Matriz[i,j],"]", Sin Saltar;	
			SiNo
				Escribir "[",Matriz[i,j],"]", Sin Saltar;
			FinSi
			
		FinPara
		Escribir "";
	FinPara
	//Punto b:
	Repetir
		Escribir "Ingrese un numero: [Entre 0 y 20] ",Sin Saltar;
		Leer num;
		Si num<=0 o num >=20 Entonces
			Escribir "Error: Numero Incorrecto";
		FinSi
	Hasta Que num>=0 Y num <=20;
	
	Para i<-0 Hasta 9 Con Paso 1 Hacer
		Para j<-0 Hasta 9 Con Paso 1 Hacer
			Si Matriz[i,j]=num Entonces
				flag<-1;
			FinSi
		FinPara
	FinPara
	Si flag=1 Entonces
		Escribir "El numero ingresado ",num," se encuentra en la matriz";
	SiNo
		Escribir "El numero ingresado ",num," no se encuentra en la matriz";
	FinSi
	//Punto c:
	Escribir "Que posicion decea cambiar: ";
	Escribir "Fila: " Sin Saltar;
	Leer i;
	Escribir "Columna: " Sin Saltar;
	Leer j;
	
	Repetir
		Escribir "Ingrese un numero: [Entre 0 y 20] ",Sin Saltar;
		Leer num;
		Si num<=0 o num >=20 Entonces
			Escribir "Error: Numero Incorrecto";
		FinSi
	Hasta Que num>=0 Y num <=20;
	
	Matriz[i-1,j-1]<-(num);
	Limpiar Pantalla;
	Escribir "Matriz:";
	Para i<-0 Hasta 9 Con Paso 1 Hacer
		Para j<-0 Hasta 9 Con Paso 1 Hacer
			SI Matriz[i,j]< 10 Entonces
				Escribir "[0",Matriz[i,j],"]", Sin Saltar;	
			SiNo
				Escribir "[",Matriz[i,j],"]", Sin Saltar;
			FinSi
			
		FinPara
		Escribir "";
	FinPara
	//Punto d:
	Para k<-0 Hasta 20 Con Paso 1 Hacer
		num<-k;
		contador<-0;
		Para i<-0 Hasta 9 Con Paso 1 Hacer
			Para j<-0 Hasta 9 Con Paso 1 Hacer
				Si (num=Matriz[i,j])  Entonces
					contador<-contador+1;
				FinSi
				FinPara
			FinPara
		Si contador!=0 Entonces
			Escribir "El numero ",num," se repite ",contador," veces";
		FinSi
	FinPara

	
FinProceso
