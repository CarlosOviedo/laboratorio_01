Proceso Matriz3x3
	Definir Matriz,i,j,aux,Traspuesta Como Entero;
	Dimension Matriz[3,3],Traspuesta[3,3];
	
	//Carga de Matriz
	Para i<-0 Hasta 2 Con Paso 1 Hacer
		Para j<-0 Hasta 2 Con Paso 1 Hacer
			Escribir "Ingrese el numero de la posicion [",i+1,",",j+1,"]", Sin Saltar;
			Leer Matriz[i,j];
		FinPara
	FinPara
	//Muestra Matriz
	Limpiar Pantalla;
	Escribir "La Matriz Cargada";
	Para i<-0 Hasta 2 Con Paso 1 Hacer
		Para j<-0 Hasta 2 Con Paso 1 Hacer
			Escribir Matriz[i,j], Sin Saltar;
		FinPara
		Escribir "";
	FinPara
	//Calculo de Traspuesta
	Para i<-0 Hasta 2 Con Paso 1 Hacer
		Para j<-0 Hasta 2 Con Paso 1 Hacer
			Traspuesta[j,i]<-Matriz[i,j];
		FinPara
	FinPara
	Escribir "La Matriz Traspuesta";
	Para i<-0 Hasta 2 Con Paso 1 Hacer
		Para j<-0 Hasta 2 Con Paso 1 Hacer
			Escribir Traspuesta[i,j], Sin Saltar;
		FinPara
		Escribir "";
	FinPara
FinProceso
