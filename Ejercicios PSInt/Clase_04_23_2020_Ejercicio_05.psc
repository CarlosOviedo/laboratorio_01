Funcion es_bisiesto <- bisiesto (anio)
	definir es_bisiesto Como Logico;
	Definir  bisiesto4, bisiesto100, bisiesto400 Como Logico;
	bisiesto4   <- anio mod 4 = 0;
	bisiesto100 <- anio mod 100=0;
	bisiesto400 <- anio mod 400=0;
	
	Si (bisiesto4 = Verdadero y bisiesto100 = Falso) o bisiesto400 = Verdadero Entonces
		es_bisiesto <- Verdadero;
	SiNo
		es_bisiesto <- Falso;
	FinSi
	
FinFuncion


Proceso DiasPorMes
	Definir Mes, anio Como Entero;
	
	Escribir "Escribir el Mes en numero (Enero: 1, Febrero 2,...,etc)";
	Leer Mes;
	
	
	Segun mes hacer
		1,3,5,7,8,10,12:
			Escribir "El mes tiene 31 Dias";
		2:
			Escribir "Escribir el A�o:";
			Leer anio;
			Si bisiesto(anio) = Verdadero Entonces
				Escribir "El A�o es Bisiesto";
				Escribir "Tiene 29 Dias";
			SiNo
				Escribir "El A�o es no es Bisiesto";
				Escribir "Tiene 28 Dias";
			FinSi
		4,6,9,11:
			Escribir "El mes tiene 31 Dias";
		De Otro Modo:
			Escribir "ERROR: El Valor ingresado es incorrecto";
	FinSegun
	
	
FinProceso
