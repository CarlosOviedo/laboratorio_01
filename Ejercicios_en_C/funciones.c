#include <stdio.h>
#include <math.h>
float potencia (float, int);
float raiz(float);
int main ()
{
 int i;
 float aux_pot,aux_raiz;
 printf ("El programa calcula las potencias de PI desde la potencia 2 hasta la potencia 10 
          y la raiz cuadrada de dichas potencias \n");

for(i=2;i<=10;i++)
{
 aux_pot = potencia (M_PI, i);
 aux_raiz= raiz(aux_pot);
 printf ("%0.3f^%d: %0.3f -> %0.3f^(1/2): %0.3f \n",M_PI,i,aux_pot,aux_pot,aux_raiz);
}

return 0;
}
float potencia (float pi, int pot)
{
  return pow(pi,pot);
}
float raiz(float a)
{
 return sqrt(a);
 }