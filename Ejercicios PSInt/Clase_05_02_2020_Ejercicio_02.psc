Proceso Arreglo_Aleatorio
	Definir Vector,i Como Entero;
	Dimension Vector[20];
	
	//Carga de Vector
	Para i<-0 Hasta 19 Con Paso 1 Hacer
		Vector[i]<-ALEATORIO(-10,10); 
	FinPara
	// Muestra del Vector
	Para i<-0 Hasta 19 Con Paso 1 Hacer
		Escribir "Vector [",i,"]: ",Vector[i];
	FinPara
	
	
FinProceso
