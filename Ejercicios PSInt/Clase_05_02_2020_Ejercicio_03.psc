Proceso Arreglo_Aleatorio
	Definir Vector,i,positivo,negativo Como Entero;
	
	Dimension Vector[20];
	negativo<-0;
	positivo<-0;
	//Carga de Vector
	Para i<-0 Hasta 19 Con Paso 1 Hacer
		Vector[i]<-ALEATORIO(-10,10); 
	FinPara
	// Muestra del Vector
	Para i<-0 Hasta 19 Con Paso 1 Hacer
		Escribir "Vector [",i,"]: ",Vector[i];
	FinPara
	// Cuenta de Negativo y Positivos
	Para i<-0 Hasta 19 Con Paso 1 Hacer
		Si Vector[i]<0 Entonces
			negativo<-negativo+1;
		FinSi
		Si Vector[i]>0 Entonces
			positivo<-positivo+1;
		FinSi
	FinPara
	Escribir "";
	Escribir "Positivo: " Sin Saltar;
	Para i<-1 Hasta positivo Con Paso 1 Hacer
		Escribir "*",Sin Saltar;
	FinPara
	Escribir "";
	Escribir "Negativo: " Sin Saltar;
	Para i<-1 Hasta negativo Con Paso 1 Hacer
		Escribir "*",Sin Saltar;
	FinPara
	Escribir "";
FinProceso
