Proceso Notas
	Definir  NOTA Como Caracter;
	Definir  Valor_Nota Como Entero;
	
	Valor_Nota<-0;
	Escribir "Ingrese la Nota del Alumno ( A a F)";
	Leer NOTA;
	
	Si NOTA = 'A' Entonces	Valor_Nota<-10;
	FinSi
	Si Nota = 'B' Entonces	Valor_Nota<-8;
	FinSi
	Si Nota = 'C' Entonces	Valor_Nota<-6;
	FinSi
	Si Nota = 'D' Entonces	Valor_Nota<-4;
	FinSi
	Si Nota = 'F' Entonces	Valor_Nota<-2;
	FinSi

	
	Segun Valor_Nota Hacer
		10:
			Escribir "Excelente";
		8:
			Escribir "Buena";
		6:
			Escribir "Regular";
		4:
			Escribir "Suficiente";
		2:
			Escribir "No Suficiente";
		De Otro Modo:
			Escribir "ERROR: Nota Incorrecta";
	FinSegun
FinProceso
