#include <stdio.h>
#include <stdlib.h>
#include <time.h>
     

int main ()
{
 int i, Vector[20],n_pares,n_impares;

 n_pares=0;
 n_impares=0;
 srand (time(NULL));      


 printf("El Vector Cargado es:\n");
 for(i=0;i<20;i++)
 {
   Vector[i]=rand();
   printf("Vector[%d]: %d \t",i,Vector[i]);
   if(!(i%5))printf("\n");
   if(Vector[i]%2) n_pares++;
   else n_impares++;
 }

 printf("\nEl Vector tiene:\nN Pares:%d\nN Impares: %d",n_pares,n_impares);
 return 0;
}