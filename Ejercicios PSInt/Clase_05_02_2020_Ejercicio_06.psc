Proceso OrdenArreglo
	Definir Vector,i,j,aux Como Entero;
	Dimension Vector[10];
	Escribir "Cargar un vector con 10 numeros";
	Para i<-0 Hasta 9 Con Paso 1 Hacer
		Escribir "Vector [",i+1,"]: ",Sin Saltar;
		Leer Vector[i];
	FinPara

	
	//Ordenamiento de Menor a Mayor
	
	Para i<-0 Hasta 9 Con Paso 1 Hacer
		Para j<-0 Hasta 9-1 Con Paso 1 Hacer
			Si Vector[j]>Vector[j+1] Entonces
				aux	<-Vector[j];
				Vector[j]<-Vector[j+1];
				Vector[j+1]<-aux;
			FinSi
		FinPara
	FinPara
		
	Escribir "El Vector Ordenado";
	Para i<-0 Hasta 9 Con Paso 1 Hacer
		Escribir "Vector [",i+1,"]: ",Vector[i];
	FinPara	
		

FinProceso
