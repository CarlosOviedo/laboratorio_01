#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
// Necesaria para incluir el strcpy
#include <string.h>

struct depto{
  int  DEPTO_ID;
  char NOMBRE      [12];
  char DIRECTOR    [12];
  char DESCRIPCION [100];
  int  TELEFONO;
};

struct profesor {
 int  PROF_ID;
 char NOMBRE   [12];
 char APELLIDO [12];
 int  TELEFONO;
 
};

struct curso {
  int  CURSO_ID;
  int  PROF_ID;
  char NOMBRE      [12];
  char NIVEL       [12];
  char DESCRIPCION [100];
};

int main()
{
 struct profesor Carlos;
 struct curso PrimerI;
 struct depto Basicas;
 
 Carlos.PROF_ID=01;
 strcpy(Carlos.NOMBRE,"Carlos");
 strcpy(Carlos.APELLIDO,"Oviedo");
 Carlos.TELEFONO=4423755;
 
 PrimerI.CURSO_ID=01;
 PrimerI.PROF_ID=01;
 strcpy(PrimerI.NOMBRE,"Primero I");
 strcpy(PrimerI.NIVEL,"Secundaria");
 strcpy(PrimerI.DESCRIPCION,"Secundaria - Primer Anio - I");

 Basicas.DEPTO_ID=02;
 strcpy(Basicas.NOMBRE,"Basicas");
 strcpy(Basicas.DIRECTOR,"Roberto");
 Basicas.TELEFONO= 44552233;
 strcpy(Basicas.DESCRIPCION,"Matematicas,Lengua,Historia");
 

 printf("Profesor: \nID:%d \nNombre: %s\nApellido: %s\nTelefono: %d",Carlos.PROF_ID,Carlos.NOMBRE,Carlos.APELLIDO,Carlos.TELEFONO);
 printf("\n");
 printf("Curso: \nID:%d \nNombre: %s\nNivel: %s\nDescripcion: %s",PrimerI.CURSO_ID,PrimerI.NOMBRE,PrimerI.NIVEL,PrimerI.DESCRIPCION);
 printf("\n");
 printf("Depto: \nID:%d \nNombre: %s\nDirector: %s\nTELEFONO: %d\nDescripcion: %s",Basicas.DEPTO_ID,Basicas.NOMBRE,Basicas.DIRECTOR,Basicas.TELEFONO,Basicas.DESCRIPCION);

 
 return 0;
}
