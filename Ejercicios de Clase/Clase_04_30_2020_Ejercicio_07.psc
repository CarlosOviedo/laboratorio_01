Proceso CargaVector
	Definir Vector,Num,i Como Entero;
	
	Dimension Vector[20];
	
	Para i<-0 Hasta 19 Con Paso 1 Hacer
		Vector[i] <- Aleatorio(-10,10) ;
	FinPara
	
	Escribir "El Vector cargado es:";
	Para i<-0 Hasta 19 Con Paso 1 Hacer
		Escribir "Vector[",i+1,"]: ",Vector[i] ;
	FinPara
FinProceso
