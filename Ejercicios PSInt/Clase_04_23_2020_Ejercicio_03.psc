Proceso Promedio
	Definir SUMA, PROM,Aux Como Real;
	Definir i,n Como Entero;
	
	Suma<-0;
	Aux<-0;
	Prom <-0;
	n<-0;
	
	Escribir "Ingrese cuantos datos va a cargar";
	Leer n;
	
	Para i<-1 Hasta n Con Paso 1 Hacer
		Escribir "Ingrese el dato (i)",i;
		Leer Aux;
		Suma <- Suma + Aux;
	FinPara
	
	Prom <- Suma/n;
	Escribir "El promedio de los numeros ingresado es: ",Prom;
FinProceso
