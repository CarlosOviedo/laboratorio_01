#include <stdio.h>

int main ()
{
    int a=10;
    int *p;
    p=&a;

    printf("*p: %d\n",*p);
    printf("&a: %p\n",&a);
    printf("p: %p\n",p);
    printf("&p: %p\n",&p);

}