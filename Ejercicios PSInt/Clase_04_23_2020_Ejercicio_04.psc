Proceso Suma
	Definir Max, Min, sumatoria,i como Entero;
	Escribir "Ingrese dos numeros enteros para realizar una suma de todos los numeros entre ellos";
	Escribir "Max:";
	Leer Max;
	Escribir "Min:";
	Leer Min;
	sumatoria<-0;
	Para i<-(Min+1) Hasta (Max-1) Con Paso 1 Hacer
		sumatoria<-sumatoria + i;
		Escribir i sin saltar;
		si i != (Max-1) Entonces
			Escribir "+" sin saltar;
		FinSi
	FinPara
	Escribir " = ", sumatoria;
FinProceso