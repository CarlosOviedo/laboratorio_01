Proceso Arreglo
	Definir Vector,i,sum,menor,mayor Como Entero;
	Definir prom como Real;
	Dimension Vector[5];
	sum <-0;
	prom<-0;
	Escribir "Ingrese 5 Valores Enteros";
	
	//Carga de Vector
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		Escribir "Vector [",i,"]: ",Sin Saltar;
		Leer Vector[i];
	FinPara
	//Calculo de Promedio
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		sum <- sum + Vector[i];
	FinPara
	prom<-sum/5;
	
	//Calculo del Menor y Mayor
	menor<-Vector[0];
	mayor<-Vector[0];
	Para i<-0 Hasta 4 Con Paso 1 Hacer
		Si mayor < Vector[i] Entonces
			mayor<-Vector[i];	
		FinSi
		Si menor > Vector[i] Entonces
			menor<-Vector[i];	
		FinSi
	FinPara
	
	//Muestra en Pantalla.
	Escribir "El Promedio de los numeros ingresados es: ",prom;
	Escribir "El mayor numero ingresado es: ",mayor;
	Escribir "El menor numero ingresado es: ",menor;
FinProceso
