#include <stdio.h>
#include <limits.h>

int main ()
{

 printf ("Tipo \t\t Tananio \t Minimo \t Maximo\n");
 printf ("char \t\t %lu       \t %d   \t %d\n",sizeof(char),CHAR_MIN,CHAR_MAX);
 printf ("uchar \t\t %lu       \t %d   \t %d\n",sizeof(char),0,UCHAR_MAX);
 printf ("short \t\t %lu       \t %d   \t %d\n",sizeof(short),SHRT_MIN,SHRT_MAX);
 printf ("int \t\t %lu \t\t \n",sizeof(int));
 printf ("long \t\t %lu \t\t \n",sizeof(long));
 printf ("long long \t %lu \t\t \n",sizeof(long long));
 printf ("float \t\t %lu \t\t \n",sizeof(float));
 printf ("double \t\t %lu \t\t \n",sizeof(double));
 printf ("long double \t %lu \t\t \n",sizeof(long double));
 return 0;
}
